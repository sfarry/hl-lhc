# -*- coding: utf-8 -*-
import xlrd, requests, urllib

#wg1 - should be obsolete
#link = 'https://docs.google.com/spreadsheets/d/1nWuaYoP06HdjJTvfAT72WwyZKgc9ZHRH54qQGLEv-qg/export'
#main

#would be easier to use csv eventually
link = 'https://docs.google.com/spreadsheets/d/167WsB2xCklZalMwqlDOZqb3BKUZTKQFi5jlz2YRt6ak/export'
response = urllib.urlretrieve(link)
ws = xlrd.open_workbook(response[0])

print "Got work book"

#any addresses which should be used even if an address has been chosen using another entry
force_address = []

#ws = xlrd.open_workbook(file_name)
#sheet = ws.sheets()[0]

#ws = xlrd.open_workbook('/Users/sfarry/Downloads/WG1 ONLY - HL%2FHE Physics report signup sheet.xlsx')
main = ws.sheets()[0]
#lhcb  = ws.sheets()[1]
from operator import attrgetter
from pylatexenc import latexencode, latex2text

sheets = [ main ]

authors = {}
steering_committee = {}
wg1_authors = {}
wg1_conveners = {}
wg2_authors = {}
wg2_conveners = {}
wg3_authors = {}
wg3_conveners = {}
wg4_authors = {}
wg4_conveners = {}
wg5_authors = {}
wg5_conveners = {}

#add author to list
def add(author, authors):
    if author.inspire_id in authors.keys():
        #if author already exists, but with a different institute, add new institute to existing record
        existing = authors[inspire_id]
        for institute in author.inspire_institutes:
            if institute not in existing.inspire_institutes:
                existing.inspire_institutes += [ institute ]
                #print 'adding extra institute %i from %s, %s to %s, %s'%(institute, existing.last_name, existing.inspire_id, author.last_name, author.inspire_id)
    else:
        authors[author.inspire_id] = author

class author:
    def __init__(self, inspire_ids, last_name, initial, institutes, inspire_name, inspire_institutes):
        self.last_name = last_name
        self.initial = initial
        self.institutes = institutes
        self.inspire_id = inspire_id
        #name as kept by inspire
        self.inspire_name = inspire_name
        #keys of inspire institutes
        self.inspire_institutes = inspire_institutes
        self.sort_name = last_name.replace('de ','') + ' ' + initial

#get institute id from inspire, or from missing institutes if it's there
#not needed currently
def get_institute(institute_id):
    if institute_id in missing_institutes:
        institute = missing_institutes[institute_id].decode('utf-8')
    else:
        r = requests.get('http://inspirehep.net/record/'+str(institute_id)+'?of=recjson&ot=corporate_name')
        institute = ""
        if r.ok:
            json = r.json()
            if len(json) > 0 and 'corporate_name' in json[0].keys() and json[0]['corporate_name'] is not None:
                details = json[0]['corporate_name'][0]
                if 'name' in details and details['name'] is not None:
                    if 'subordinate_unit' in details and details['subordinate_unit'] is not None:
                        institute = details['subordinate_unit'] + ', '
                    institute  = institute + details['name']
                else:
                    print "Can't find a name for ",institute_id
                    print details, json
            else:
                print "json not okay for ",institute_id
                
        else:
            print "Can't find result for ",institute_id
    #texname = latexencode.utf8tolatex(institute.decode('utf-8'))
    
    textname = latex2text.latex2text(institute)
    texname = latexencode.utf8tolatex(textname)
    return texname


#convert list of authors and conveners with their institutes to tex
def get_tex_authors(convener_list, author_list, output):
    author_tex   = ''
    convener_tex = ''
    institute_tex = ''
    author_institutes = bidict()

    #fill author list first
    ins_key = 1
    for a in convener_list:
        if convener_tex != '':
            convener_tex += ', '
        name = a.initial + ' ' + a.last_name
        #some are in latex, some not, so easiest is to convert from latex first, and then back
        textname = latex2text.latex2text(name.decode('utf-8'))
        texname = latexencode.utf8tolatex(textname)
        #texname = latexencode.utf8tolatex(name.decode('utf-8'))
        #convener_list += '\\href{http://inspirehep.net/record/%s}{%s}'%(a.inspire_id, texname)
        convener_tex += texname

        ninst = 0
        convener_tex +='$^{'
        for institute in a.inspire_institutes:
            if institute not in author_institutes.keys():
                author_institutes[institute] = ins_key
                ins_key = ins_key + 1
            if ninst == 0:
                convener_tex += '%i'%(author_institutes[institute])
            else:
                convener_tex += ',%i'%(author_institutes[institute])
            ninst += 1
        convener_tex+='}$\n'

    for a in author_list:
        if author_tex != '':
            author_tex += ', '
        name = a.initial + ' ' + a.last_name
        textname = latex2text.latex2text(name.decode('utf-8'))
        texname = latexencode.utf8tolatex(textname)
        if a.inspire_id != '':
            #author_list += '\\href{http://inspirehep.net/record/%s}{%s}'%(a.inspire_id, texname)
            author_tex += '%s'%(texname)
        else:
            author_tex += '%s'%(texname)
        ninst = 0
        author_tex +='$^{'
        for institute in a.inspire_institutes:
            if institute not in author_institutes.keys():
                author_institutes[institute] = ins_key
                ins_key += 1
            if ninst == 0:
                author_tex += '%i'%(author_institutes[institute])
            else:
                author_tex += ',%i'%(author_institutes[institute])
            ninst += 1
        author_tex+='}$\n'

    #now fill institute list
    for i in range(1,len(author_institutes)+1):
        #get the institute corresponding to the key
        institute = author_institutes.inv[i]
        if institute_tex != '':
            institute_tex += ', '
        #institute_list += '$^{%i}$ \\href{http://inspirehep.net/record/%i}{%s}\n'%(i, institute, get_institute(institute))
        institute_tex += '$^{%i}$~%s\n'%(i, institutes[institute])
    
    f = open(output+'.tex', 'w')
    f.write('\\author{Editors: \\\\')
    f.write(convener_tex.encode('utf-8'))
    f.write('''\\\\ \\vspace*{4mm} 
Contributors: 
\\\\ ''')
    f.write(author_tex.encode('utf-8'))
    f.write('''\\vspace*{1cm} 
}
\\institute{''')
    f.write(institute_tex.encode('utf-8'))
    f.write('}')
    f.close()

from bidict import bidict

#convener names, would be better to use ids but anyway...
steering_names = [ 'Mangano', 'Nisati', 'Dainese', 'Meyer', 'Salam', 'Vesterinen' ]
wg1_convener_names = ['Azzi', 'Tricoli', 'Zeppenfeld', 'Farry', 'Nason']
wg2_convener_names = ['Gori', 'Riva', 'Kado', 'Cepeda', 'Ilten']
wg3_convener_names = ['Fox', 'Torre', 'D\'Onofrio', 'Ulmer', 'Cid Vidal']
wg4_convener_names = ['Martin Camalich', 'Zupan', 'Cerri', 'Malvezzi', 'Gligorov']
wg5_convener_names = ['Weidemann', 'Grosse-Oetringhaus', 'Citron', 'Winn', 'Lee', 'Jowett']


#let's get all the institutes, key will be inspire id, take first address in case of duplicates

#we'll reserve some addresses, let's keep a separate dictionary for those so we know their unique id in main
reserved_addresses = {}

unique_institute_id = 1

institutes = {}
for sheet in sheets:
    for row in range(1,sheet.nrows):
        ids = []
        addresses = []
        val1 = sheet.cell(row,3).value
        val2 = sheet.cell(row,4).value
        if val1 == '' or val1 == 0 or val1 == '0':
            ids = [unique_institute_id]
            addresses = [ val2 ]
            reserved_addresses[val2] = unique_institute_id
            unique_institute_id+=1
        elif type(val1) == float or type(val1) == int:
            ids = [ int(val1) ]
            addresses = [ val2.replace('\n','') ]
        elif type(val1) == unicode or type(val1) == str:
            ids = [int(v) for v in val1.split(',')]
            addresses = val2.split(';')
            if len(ids) != len(addresses):
                print 'author %s %s has %i institutes but %i addresse(s)'%(sheet.cell(row,1).value, sheet.cell(row,0).value, len(ids), len(addresses))
                addresses = [ val2 for i in range(len(ids)) ]
        else: print 'something I haven\'t thought of?', type(val1)
        for val,addr in zip(ids, addresses):
            textname = latex2text.latex2text(addr.replace('\n',''))
            texname = latexencode.utf8tolatex(textname)
            if val not in institutes.keys():
                institutes[ val ] = texname
            else:
                #if it is in force address, is already here, and hasn't been marked already
                #check that we have the right address, otherwise we make it a new entry
                if texname in force_address and texname not in reserved_addresses and texname != institutes[val]:
                    institutes[unique_institute_id] = texname
                    reserved_addresses[ texname ] = unique_institute_id
                    unique_institute_id += 1
            #    print 'for institute id %i, %s is not equal to %s for %s'%(val, addr, institutes[val], sheet.cell(row,0).value)
                        
print "Got %i institutes in total"%(len(institutes))
                    
#now get all authors
#unique key will be inspire id
#for users without inspire id, incremental integer as key

unique_author_id = 1
for sheet in sheets:
    for row in range(1,sheet.nrows):
        inspire_id = sheet.cell(row,2).value
        if inspire_id == '' or inspire_id == 0 or inspire_id == "0":
            inspire_id = unique_author_id
            unique_author_id += 1
        if type(inspire_id) is unicode and 'INSPIRE-' in inspire_id:
            print 'need to fix ',inspire_id
        elif type(inspire_id) == int:
            inspire_id = str(inspire_id)
        elif type(inspire_id) == float:
            inspire_id = str(int(inspire_id))

        #get inspire name with API - not strictly needed
        #r = requests.get('http://inspirehep.net/record/'+inspire_id+'?of=recjson&ot=recid,authors,title')
        inspire_name = ''
        author_institutes = []
        #look up if we have stored the address to keep
        addr = sheet.cell(row,4).value
        textname = latex2text.latex2text(addr.replace('\n',''))
        texname = latexencode.utf8tolatex(textname)
        if texname in reserved_addresses.keys():
            author_institutes = [ reserved_addresses[sheet.cell(row,4).value] ]
        elif sheet.cell(row,3).value != '' and sheet.cell(row,3).value != 0 and sheet.cell(row,3) != '0':
            if type(sheet.cell(row,3).value) == float:
                author_institutes = [ int(sheet.cell(row,3).value) ]
            elif type(sheet.cell(row,3).value) == unicode:
                author_institutes = [int(a) for a in sheet.cell(row,3).value.split(',')]

        #if r.ok:
        #    json = r.json()
        #    if len(json) > 0 and 'authors' in json[0].keys() and json[0]['authors'] is not None and len(json[0]['authors']) > 0:
        #        inspire_name = json[0]['authors'][0]['full_name']

        inspire_name = 'test'
        a = author(inspire_id, str(sheet.cell(row,0).value.encode('utf-8')), str(sheet.cell(row,1).value), str(sheet.cell(row,4)), inspire_name, author_institutes)

        #check which working groups author is in
        wg1, wg2, wg3, wg4, wg5 = False, False, False, False, False
        if sheet.cell(row,5).value == 'y': wg1 = True
        if sheet.cell(row,6).value == 'y': wg2 = True
        if sheet.cell(row,7).value == 'y': wg3 = True
        if sheet.cell(row,8).value == 'y': wg4 = True
        if sheet.cell(row,9).value == 'y': wg5 = True

        #if in any add to main list
        if wg1 or wg2 or wg3 or wg4 or wg5:
            if a.last_name in steering_names:
                add(a, steering_committee)
            else:
                add(a, authors)
        #now add to individual groups
        if wg1:
            if a.last_name in wg1_convener_names:
                add(a, wg1_conveners)
            else:
                add(a, wg1_authors)
        if wg2:
            if a.last_name in wg2_convener_names:
                add(a, wg2_conveners)
            else:
                add(a, wg2_authors)
        if wg3:
            if a.last_name in wg3_convener_names:
                add(a, wg3_conveners)
            else:
                add(a, wg3_authors)
        if wg4:
            if a.last_name in wg4_convener_names:
                add(a,wg4_conveners)
            else:
                add(a, wg4_authors)
        if wg5:
            if a.last_name in wg5_convener_names:
                add(a, wg5_conveners)
            else:
                add(a, wg5_authors)

print 'Got %i Unique Authors'%(len(authors))

author_list = authors.values()
author_list.sort(key=attrgetter('sort_name'), reverse = False)
steering_list = steering_committee.values()
steering_list.sort(key=attrgetter('sort_name'), reverse = False)

wg1_author_list = wg1_authors.values()
wg1_author_list.sort(key=attrgetter('sort_name'), reverse = False)
wg1_convener_list = wg1_conveners.values()
wg1_convener_list.sort(key=attrgetter('sort_name'), reverse = False)

wg2_author_list = wg2_authors.values()
wg2_author_list.sort(key=attrgetter('sort_name'), reverse = False)
wg2_convener_list = wg2_conveners.values()
wg2_convener_list.sort(key=attrgetter('sort_name'), reverse = False)

wg3_author_list = wg3_authors.values()
wg3_author_list.sort(key=attrgetter('sort_name'), reverse = False)
wg3_convener_list = wg3_conveners.values()
wg3_convener_list.sort(key=attrgetter('sort_name'), reverse = False)

wg4_author_list = wg4_authors.values()
wg4_author_list.sort(key=attrgetter('sort_name'), reverse = False)
wg4_convener_list = wg4_conveners.values()
wg4_convener_list.sort(key=attrgetter('sort_name'), reverse = False)

wg5_author_list = wg5_authors.values()
wg5_author_list.sort(key=attrgetter('sort_name'), reverse = False)
wg5_convener_list = wg5_conveners.values()
wg5_convener_list.sort(key=attrgetter('sort_name'), reverse = False)


#use the lists to make tex files
get_tex_authors(steering_list, author_list, 'authors')
get_tex_authors(wg1_convener_list, wg1_author_list, 'wg1_authors')
get_tex_authors(wg2_convener_list, wg2_author_list, 'wg2_authors')
get_tex_authors(wg3_convener_list, wg3_author_list, 'wg3_authors')
get_tex_authors(wg4_convener_list, wg4_author_list, 'wg4_authors')
get_tex_authors(wg5_convener_list, wg5_author_list, 'wg5_authors')
